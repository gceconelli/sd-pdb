from airflow.models import Variable
import json
import os

class DataPayload:
    def __init__(self, **kwargs):
        self.techNames = kwargs.get('techNames')
        self.isMultivendor = kwargs.get('isMultivendor')
        self.reportAggregation = kwargs.get('reportAggregation')
        self.timeAggregation = kwargs.get("timeAggregation")
        self.dateFilter = kwargs.get('dateFilter')
        self.selectedElements = kwargs.get('selectedElements')
        self.kpis = kwargs.get('kpis')
        self.customKpis = []
        self.lookupTables = kwargs.get('lookupTables')
    
    def toJson(self):
        return json.dumps(self, default=lambda o: o.__dict__)

    class ReportAggregation:
        def __init__(self, **kwargs):
            if(kwargs.get("ElementAggregationType") == 0):
                self.aggregationType = "Region"
                self.aggregationName = "Region"
                self.aggregationLevel = 0

            if(kwargs.get("ElementAggregationType") == 1):
                self.aggregationType = "Region"
                self.aggregationName = "Subregion"
                self.aggregationLevel = kwargs.get('ElementAggregationInfo')

            if(kwargs.get("ElementAggregationType") == 2):
                self.aggregationType = "Element"
                self.aggregationName = kwargs.get('ElementAggregationInfo')
                self.aggregationLevel = 0

            if(kwargs.get("ElementAggregationType") == 3):
                self.aggregationType = "EntireSet"
                self.aggregationName = "NETWORK"
                self.aggregationLevel = 0
    
    class DateFilter:
        def __init__(self, **kwargs):
            self.from_ = kwargs.get("dateFrom")
            self.to = kwargs.get("dateTo")
    
    class SelectedElements:
        def __init__(self, **kwargs):
            self.type = kwargs.get('type')
            self.tech = kwargs.get('vendor')
            self.selectAllElements = kwargs.get('selectAllElements')
            self.elementsList = kwargs.get('elementsList')
    
    class Kpis:
        def __init__(self, **kwargs):
            self.techName = kwargs.get('techName')
            self.id = kwargs.get('id')
            self.reportFormatRelation = self.ReportFormatRelation(techName=self.techName, id=self.id)
    
        class ReportFormatRelation:
            def __init__(self, **kwargs):
                self.singleVendorColumnName = kwargs.get('techName') + '_' + kwargs.get('id')
                self.multivendorColumnName = kwargs.get('id')
    
    class LookupTables:
        def __init__(self, **kwargs):
            self.tableName = "o_cells_last"
            self.lookupRelationType = kwargs.get('lookupRelationType')