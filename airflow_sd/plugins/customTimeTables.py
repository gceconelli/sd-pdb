from pendulum import UTC, Date, DateTime, Time, instance as pendulum_instance, timezone
from dateutil.relativedelta import relativedelta
from datetime import timedelta
from airflow.plugins_manager import AirflowPlugin
from airflow.timetables.base import DagRunInfo, DataInterval, TimeRestriction, Timetable
from airflow.hooks.postgres_hook import PostgresHook

UTC = timezone("UTC")
class CustomIntervalTimetable(Timetable):
    def __init__(self, CustomPeriod_RepeatPeriod, CustomPeriod_RepeatInterval, weekDays = [0]):
        self.CustomPeriod_RepeatPeriod = CustomPeriod_RepeatPeriod
        self.CustomPeriod_RepeatInterval = CustomPeriod_RepeatInterval
        self.weekDays = weekDays

    @property
    def summary(self) -> str:
        return "Custom Timetable"

    def serialize(self):
        return dict(CustomPeriod_RepeatPeriod=self.CustomPeriod_RepeatPeriod,
                    CustomPeriod_RepeatInterval=self.CustomPeriod_RepeatInterval,
                    weekDays = self.weekDays)

    @classmethod
    def deserialize(cls, value):
        return cls(**value)
    
    @staticmethod
    def find_next_day_DateTime(arr, start_date):
        def get_next_day(day):
            days_ahead = (day - start_date.weekday() + 7) % 7
            return start_date + timedelta(days=days_ahead)
        next_days = [get_next_day(day) for day in arr]
        earliest_next_day_DateTime = min(next_days)
        return earliest_next_day_DateTime
    
    @staticmethod
    def are_in_same_week(DateTime1, DateTime2):
        year1, week1, _ = DateTime1.isocalendar()
        year2, week2, _ = DateTime2.isocalendar()
        return year1 == year2 and week1 == week2
    
    @staticmethod 
    def weeks_difference(DateTime1, DateTime2):
        difference = DateTime2 - DateTime1
        weeks = difference.total_seconds() / (7 * 24 * 60 * 60)
        return int(weeks)
    def infer_manual_data_interval(self, run_after: DateTime) -> DataInterval:
        start = DateTime.combine((run_after), Time.min).replace(tzinfo=UTC)
        return DataInterval(start=start, end=start)
    def next_dagrun_info(
        self,
        *,
        last_automated_data_interval,
        restriction: TimeRestriction,
    ):
        next_start = None
        if last_automated_data_interval is not None:  # There was a previous run on the regular schedule.
            last_start = last_automated_data_interval.start
            if (self.CustomPeriod_RepeatPeriod == 0):
                next_start = (last_start + timedelta(hours=self.CustomPeriod_RepeatInterval)).replace(
                    tzinfo=UTC
                )
            if (self.CustomPeriod_RepeatPeriod == 1):
                next_start = (last_start + timedelta(days=self.CustomPeriod_RepeatInterval)).replace(
                    tzinfo=UTC
                )
            if (self.CustomPeriod_RepeatPeriod == 2):
                nextDay = self.find_next_day_DateTime(last_automated_data_interval, self.weekDays)
                if (self.are_in_same_week(last_automated_data_interval, nextDay)):
                    next_start = self.find_next_day_DateTime(last_automated_data_interval, self.weekDays).replace(
                    tzinfo=UTC
                )
                else:
                    while (self.weeks_difference(last_automated_data_interval, nextDay) < (self.CustomPeriod_RepeatInterval - 1)):
                        nextDay = nextDay + timedelta(weeks=1)
                    next_start = nextDay
            if (self.CustomPeriod_RepeatPeriod == 3):
                next_start = (last_start + relativedelta(months=self.CustomPeriod_RepeatInterval)).replace(
                    tzinfo=UTC
                )
            if (self.CustomPeriod_RepeatPeriod == 4):
                next_start = (last_start + timedelta(years=self.CustomPeriod_RepeatInterval)).replace(
                    tzinfo=UTC
                )
        else:  # This is the first ever run on the regular schedule.
            next_start = max(restriction.earliest, DateTime.now().replace(tzinfo=UTC))
            if (self.CustomPeriod_RepeatPeriod == 2):
                    nextDay = self.find_next_day_DateTime(last_automated_data_interval, self.weekDays).replace(tzinfo=UTC)
            else:
                    nextDay = DateTime.combine(Date.today(), Time.min).replace(tzinfo=UTC)
            next_start = next_start.replace(tzinfo=UTC)
            if (self.CustomPeriod_RepeatPeriod == None) or (self.CustomPeriod_RepeatPeriod == 5): 
                 next_start = None
        return DagRunInfo(run_after=next_start.replace(tzinfo=UTC), data_interval=DataInterval.exact(at=next_start.replace(tzinfo=UTC)))

class CustomIntervalPlugin(AirflowPlugin):
    name = "custom_interval_plugin"
    timetables = [CustomIntervalTimetable]
