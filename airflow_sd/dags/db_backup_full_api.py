import airflow
import os
import json
from airflow import DAG
from airflow.utils.state import State
from airflow.utils.dates import days_ago
from datetime import timedelta, datetime
from airflow.operators.dummy_operator import DummyOperator
from airflow.operators.python_operator import PythonOperator
from airflow.providers.ssh.operators.ssh import SSHOperator
from airflow.providers.sftp.operators.sftp import SFTPOperator, SFTPOperation
from airflow.providers.ftp.operators.ftp import FTPFileTransmitOperator, FTPOperation
from airflow.operators.python import BranchPythonOperator
from airflow.operators.bash import BashOperator
from airflow.operators.python import task, get_current_context
from airflow_provider_kafka.operators.produce_to_topic import ProduceToTopicOperator
from minio import Minio
import boto3
from airflow.operators.python import task, get_current_context
# from airflow_provider_kafka.operators import ConsumeFromTopicOperator
pdb_file = "pdb"


def upload_to_minio(**kwargs):
    filename = kwargs['dag_run'].conf['message']
    print(filename)
    s3 = boto3.client('s3',
                  endpoint_url='http://minio:9000',
                  aws_access_key_id='admin',
                  aws_secret_access_key='Gp8@568@#22')

    s3.upload_file(f'/tmp/filepath/pdb{filename}.ent.gz', 'pdbrepo', f'pdb{filename}.ent.gz')

    return filename

def check_parameters(**kwargs):
   filename = kwargs['dag_run'].conf['message']
   return filename

def prod_to_kafka(**kwargs):
  ti = get_current_context()['ti']
  filename = ti.xcom_pull(task_ids='check_params')
  print(filename)
  yield (json.dumps(filename), json.dumps(filename))

def generate_dag():
  default_args = {
      'owner': 'airflow',
      'depends_on_past': False,
      'start_date': days_ago(1),
      'email': ['airflow@example.com'],
      'email_on_failure': False,
      'email_on_retry': False,
      'retries': 0,
      'retry_delay': timedelta(minutes=5)
  }

  dag = DAG(dag_id='db_backup_full_api',
            default_args=default_args,
            schedule_interval="0 1 * * 0",
            max_active_runs=1, # can only run 1 isntance of this dag at the same time
            catchup=False, # avoid to run previous scheduled dags
            )
  
  with dag:
    start = DummyOperator(task_id='start',dag=dag)

    check = PythonOperator(
        task_id='check_params',
        python_callable=check_parameters,
        provide_context=True,
        do_xcom_push=True
    )

    copy_backup_files_to_airflow = FTPFileTransmitOperator(
      task_id="test_ftp_get",
      ftp_conn_id="pdb_ftp",
      local_filepath="/tmp/filepath/pdb{{ti.xcom_pull(task_ids='check_params')}}.ent.gz",
      remote_filepath="/pub/pdb/data/structures/all/pdb/pdb{{ti.xcom_pull(task_ids='check_params')}}.ent.gz",
      operation=FTPOperation.GET,
      create_intermediate_dirs=True,
      dag=dag)
    
    upload_minio = PythonOperator(
        task_id='upload_to_minio',
        python_callable=upload_to_minio,
        provide_context=True,
    )
    
    produce_to_kafka = ProduceToTopicOperator(
        task_id="produce_to_kafka",
        topic="baeldung_linux",
        producer_function=prod_to_kafka,
        kafka_config={"bootstrap.servers": "PLAINTEXT://kafka:9092"},
        poll_timeout=10,
    )
    
    done = BashOperator(
        task_id="done",
        bash_command='echo "Backup run successfully"')
    
    
    start >> check >> copy_backup_files_to_airflow >> upload_minio >> produce_to_kafka  >> done

    
  globals()["db_backup_full_api"] = dag

generate_dag()