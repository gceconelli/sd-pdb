import string
import dagfactory.constants as ct
from datetime import timedelta, datetime
import os

def singleton_date(local_tz):
    if os.environ.get("start_date", 'not-set') == 'not-set':
        start_date = (datetime.now(tz=local_tz) - timedelta(days=2)).strftime("%Y-%m-%d")
        os.environ["start_date"] = str(start_date)
    else:
        start_date = os.environ["start_date"]
    start_date = datetime.strptime(start_date, "%Y-%m-%d")
    start_date = datetime(start_date.year, start_date.month, start_date.day, tzinfo=local_tz)
    return start_date
  
def check_file_existence(params):
  if(os.path.isfile(ct.DAG_FOLDER + "/" + str(params["id"]) + ".py" )):
    return 0
  else:
    raise Exception("file could not be found")

def check_file_non_existence(params):
  if(os.path.isfile(ct.DAG_FOLDER + "/" + str(params["id"]) + ".py" )):
    raise Exception("file is found")
  else:
    return 0

def check_file_branchingeraser(params):
  if(os.path.isfile(ct.DAG_FOLDER + "/" + str(params["id"]) + ".py" )):
    return "remove_dag"
  else:
    return "log_missing_dag"

def trim(str):
  returnStr = str.translate({ord(c): None for c in string.whitespace})
  return returnStr