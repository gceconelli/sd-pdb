from pyspark.sql import SparkSession
from pyspark.sql.functions import expr

# Configuração da sessão Spark
spark = SparkSession.builder \
    .appName("KafkaSparkIntegration") \
    .config("spark.jars", "/opt/spark/jars/spark-sql-kafka-0-10_2.12-3.0.2.jar") \
    .getOrCreate()

# Leitura de streaming do tópico Kafka
kafka_df = spark.readStream \
    .format("kafka") \
    .option("kafka.bootstrap.servers", "localhost:9092") \
    .option("subscribe", "baeldung_linux").load()

print(kafka_df)
# Processamento das mensagens do Kafka (converter valor para string)
processed_df = kafka_df.selectExpr("CAST(value AS STRING) as message")

# Saída para o console (substitua por sua lógica de processamento real)
query = processed_df.writeStream \
    .outputMode("append") \
    .format("console") \
    .start()

# Aguardar a conclusão da consulta
query.awaitTermination()
