# -*- coding: utf-8 -*-
from confluent_kafka import Consumer, KafkaError
import subprocess
import asyncio


# Configurações do consumidor
conf = {
    'bootstrap.servers': 'localhost:29092',  # Substitua pelo endereço do seu servidor Kafka
    'group.id': '3',  # Escolha um ID de grupo exclusivo
    'auto.offset.reset': 'earliest'
}

# Crie o consumidor
consumer = Consumer(conf)

# Subscreva-se a tópicos de interesse
topics = ['baeldung_linux']  # Substitua pelo nome do seu tópico
consumer.subscribe(topics)

# Loop de leitura de mensagens
try:
    while True:
        msg = consumer.poll(3.0)  # Tempo limite de 1 segundo
        if msg is None:
            continue
        if msg.error():
            print('error')
            if msg.error().code() == KafkaError._PARTITION_EOF:
                continue
            else:
                print(msg.error())
                break
        command = f"docker exec -d spark_master /opt/spark/bin/spark-submit --master spark://spark-master:7077 --name {msg.value().decode('utf-8')} --class org.apache.spark.examples.SparkPi  local:///opt/spark-apps/read_pdb_and_insert.py {msg.value().decode('utf-8')}"
        subprocess.run(command, shell=True, capture_output=True, text=True)
        print('Mensagem recebida: {}'.format(msg.value().decode('utf-8')))
        # subprocess.check_output("docker exec spark_master /opt/spark/bin/spark-submit --master spark://spark-master:7077 --name spark-pi --class org.apache.spark.examples.SparkPi  local:///opt/spark-apps/read_pdb_and_insert.py")
        # subprocess.check_output("docker exec spark_master ls")
except KeyboardInterrupt:
    pass
finally:
    # Feche o consumidor
    consumer.close()
