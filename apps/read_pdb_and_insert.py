# Import SparkSession
from pyspark.sql import SparkSession
from pyspark.sql import Row
import os
import sys
import urllib.request
import time

import Bio
import Bio.PDB
import Bio.SeqRecord
from biopandas.pdb import PandasPdb
import boto3
from sh import gunzip

def download_from_minio(filename):
    print(type(filename))
    s3 = boto3.client('s3',
                  endpoint_url='http://minio:9000',
                  aws_access_key_id='admin',
                  aws_secret_access_key='Gp8@568@#22')

    s3.download_file('pdbrepo', f'pdb{filename}.ent.gz', f'/pdbs/pdb{filename}.ent.gz')

spark = SparkSession.builder\
        .config("spark.jars", "/opt/spark/jars/postgresql-42.6.0.jar")\
        .config("spark.executor.cores", 1)\
        .config("spark.hadoop.fs.s3a.endpoint", "http://minio:9000")\
        .config("spark.hadoop.fs.s3a.access.key", 'admin')\
        .config("spark.hadoop.fs.s3a.secret.key", 'Gp8@568@#22')\
        .config("spark.hadoop.fs.s3a.path.style.access", True)\
        .config("spark.hadoop.fs.s3a.impl", "org.apache.hadoop.fs.s3a.S3AFileSystem")\
        .getOrCreate()

pdb_file_name = sys.argv[1:]

download_from_minio(pdb_file_name[0])
gunzip(f"/pdbs/pdb{pdb_file_name[0]}.ent.gz")
os.rename(f"/pdbs/pdb{pdb_file_name[0]}.ent", f"/pdbs/{pdb_file_name[0]}.pdb")
ppdb_df =  PandasPdb().read_pdb(f"/pdbs/{pdb_file_name[0]}.pdb")
time.sleep(30)
pdb_converted_df = spark.createDataFrame(ppdb_df.df['ATOM'])

print(ppdb_df.df.keys())
# atom_df = ppdb_df.df['ATOM'].select("*")
print(pdb_converted_df.select("*"))

pdb_converted_df.select("*").write.format("jdbc")\
    .option("url", "jdbc:postgresql://demo-database:5432/pdb_files") \
    .option("driver", "org.postgresql.Driver").option("dbtable", pdb_file_name[0]) \
    .option("user", "postgres").option("password", "root").save()
